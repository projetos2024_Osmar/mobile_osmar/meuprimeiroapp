import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TouchableOpacity, ScrollView, Image } from 'react-native';
import { useEffect, useState } from 'react';

export default function App() {

  const [count, setCount] = useState(0);
  const [dado, setDado] = useState(0);

  function sortearDado(){
    setDado(Math.floor(Math.random() * 6) + 1);
  }
  return (
    
    <View style={styles.container}>
      
      <ScrollView>

      <Text style={styles.styleText}>Tigrinho App</Text>
      
      <TouchableOpacity style={styles.styleTouch} onPress={sortearDado}>
        <Text>Apostar</Text>
      </TouchableOpacity>

      <Text style={styles.styleValue}>{dado}</Text>

      <TouchableOpacity style={styles.styleTouch} onPress={() => setCount((count+1)*2)}>
        <Text>Clique para ganhar!</Text>
      </TouchableOpacity>
      
      <Text style={styles.styleValue}>R$ {count}</Text>

      <TouchableOpacity style={styles.styleTouch} onPress={() => setCount(0)}><Text>Doar tudo para o Ney</Text></TouchableOpacity>

      </ScrollView>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1EB44D',
    alignItems: 'center',
    justifyContent: 'center',
  },
  styleText: {
    fontSize: 32,
    fontWeight: 'bold',
    color: "#fff",
    textShadowColor: '#000',
    textShadowRadius: 20,
  },
  styleValue: {
    margin: 20,
    fontSize: 26,
    color: '#E5D838',
  },
  styleTouch: {
    backgroundColor: '#fc694a',
    alignItems: 'center',
    padding: 16,
    borderRadius: 9,
    marginTop: 25,
    marginBottom: 16,

  }
 
});